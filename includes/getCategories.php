<?php 

//this function takes a sql result and loops through all the comic strips
//for each comic it grabs all of it's categories and then returns a multidimentional array 
//the returned array contains an array for each comic, with each category and the categories SEO name 
//from this big ass array, each post will grab the sub array it needs, which will use the 'seoTitle' as a key

function getCategories($conn){


	$sqlCategoryGrab = "select * from lost_cause.comic_strips
	JOIN comic_and_categories ON comic_strips.id = comic_and_categories.comicID
	JOIN categories ON categories.id = comic_and_categories.categoryID
	ORDER BY comic_strips.id ";	

	$categoryResult = $conn->query($sqlCategoryGrab);


	$categoryArray = array();
	$comicID_Counter = 1;
	if ($categoryResult->num_rows > 0) {
		while($row = $categoryResult->fetch_assoc()) {
			//echo $row['title'] ." -  " .    $row['categoryName'] . " - index: " . $comicID_Counter . "<br>";
			
			/*we compare the comic id with our comicID_counter. Everytime the comicID is 1, we grab the category
			IF we only said else{ $comicID_Counter ++; }, then that first time the comicID changes, we'd miss that category
			So here in the else{}, we not only ++ the counter, but run the code again for that first iteration of the new comicID value
			This SHOULD be pretty cut and dry for future me. ... I hope so.
			All this creates the $categoryArray[] whos values we can use in the next sql call for the posts
			*/
			if($comicID_Counter == $row['comicID']){
				if( !array_key_exists($row["seoTitle"],$categoryArray) ){
					$categoryArray[$row["seoTitle"]] = array();
				}
				$newArray = array("categoryName" => $row['categoryName'], "categorySeoName" => $row['categorySeoName']);
				array_push($categoryArray[$row["seoTitle"]], $newArray );
			}
			else{
				$comicID_Counter ++;
				if( !array_key_exists($row["seoTitle"],$categoryArray) ){
					$categoryArray[$row["seoTitle"]] = array();
				}
				$newArray = array("categoryName" => $row['categoryName'], "categorySeoName" => $row['categorySeoName']);
				array_push($categoryArray[$row["seoTitle"]], $newArray );
			}

		}//end while()
	}//end if()


	return $categoryArray;

}//end getCategories()



?>