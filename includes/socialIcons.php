<?php 
//this file is included in each post
?>
<div class="socialIcons">
	

<?php if( !empty($row['deviantArtUrl']) ){ ?>

	<p>
		<a href="<?= $row['deviantArtUrl'] ?>">
			<img src="images/socialIcons/deviantArt.jpg" alt="deviantArt" title="Share '<?= $row['title'] ?>' on DeviantArt">
		</a>
	</p>

<?php } ?>


<?php if( !empty($row['tumblrUrl']) ){ ?>
	<p>
		<a href="<?= $row['tumblrUrl'] ?>">
			<img src="images/socialIcons/tumblr.jpg" alt="tumblr" title="Share '<?= $row['title'] ?>' on Tumblr"">
		</a>
	</p>
<?php } ?>



<?php if( !empty($row['instagramUrl']) ){ ?>
	<p>
		<a href="<?= $row['instagramUrl'] ?>">
			<img src="images/socialIcons/instagram.jpg" alt="instagram" title="Share '<?= $row['title'] ?>' on Instagram"">
		</a>
	</p>

<?php } ?>

	
</div>