<?php
include "includes/keymaster.php";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


function getComicsAmount(){
	global $conn;
	$query="select COUNT(*) as total from comic_strips";
	$result = $conn->query($query);		
	while($row = $result->fetch_assoc()) {
		return $row["total"];
	}
}

function getCategoryPostAmount($category = "depression"){	
	global $conn;
	$sql = "select * from lost_cause.comic_strips
			JOIN comic_and_categories ON comic_strips.id = comic_and_categories.comicID
			JOIN categories ON categories.id = comic_and_categories.categoryID

			WHERE categories.categorySeoName = '{$category}'";
	$result = $conn->query($sql);		
	
	return $result->num_rows;

}
?>	        






<?php
//////////////////////////////////////////////////////get categories for the sidebar area
function listCategories(){

	global $conn;
	$sql = "select * from categories";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
?>	        
		<p>
			<a href="category/<?= $row['categorySeoName'] ?>/1"><?= $row['categoryName'] ?></a>
		</p>

		

<?php

	    }
	} else {
	    echo "0 results";
	}
	//$conn->close();

}
//////////////////////////////////////////////////////end listCategories()
?>




<?php
//////////////////////////////////////////////////// get all the posts of category()


function getCategoryPosts($page = 0,$amount = 10, $category){
	global $conn;
	
	////use the getCategories() function to build an array of each post's categories
	$categoryArray = getCategories($conn);

	if($page == 1){
		$offsetAmount = 0;
	}
	else{
		$offsetAmount = ($page - 1) * $amount;
	}

	
	$sql = "select * from lost_cause.comic_strips
			JOIN comic_and_categories ON comic_strips.id = comic_and_categories.comicID
			JOIN categories ON categories.id = comic_and_categories.categoryID

			WHERE categories.categorySeoName = '{$category}'
			ORDER BY comic_strips.id DESC LIMIT {$amount} OFFSET {$offsetAmount}";
	

	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
?>	        
	    <div class="post">
			<img src="images/posts/<?= $row['imagePath'] ?>">
			<div class="postTopRow">
				<h2><?= $row['title'] ?></h2>
				<?php include "socialIcons.php"  ?>
			</div>
			<p><?= $row['description'] ?> </p>
			
			<p class="categories">
				Categories: 
			<?php 
			//print out categories and check for the damn last comma
				$postCategories = $categoryArray[$row['seoTitle'] ];
				$length = sizeof($postCategories);
				$counter = 1;
				foreach($postCategories as $category) {
				    echo "<a href='category/{$category['categorySeoName']}/1'>" .$category['categoryName']. "</a>";
				    if($counter < $length){
				    	echo ", ";
				    }
				    $counter++;
				}
			?>
			</p>
			<p class="date"><?= $row['date'] ?> </p>

		</div><!-- end post -->

		

<?php

	    }
	} else {
	    echo "0 results";
	}
	//$conn->close();

}
//////////////////////////////////////////////////////end getCategoryPosts()

?>















<?php
//////////////////////////////////////////////////// get all the posts()


function getAllComics($page = 0,$amount = 10){
	global $conn;
	
	////use the getCategories() function to build an array of each post's categories
	$categoryArray = getCategories($conn);

	if($page == 1){
		$offsetAmount = 0;
	}
	else{
		$offsetAmount = ($page - 1) * $amount;
	}

	
	$sql = "SELECT * FROM comic_strips ORDER BY id DESC LIMIT {$amount} OFFSET {$offsetAmount}";
	
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
?>	        
	    <div class="post">
			<img src="images/posts/<?= $row['imagePath'] ?>">
			<div class="postTopRow">
				<h2><?= $row['title'] ?></h2>
				<?php include "socialIcons.php"  ?>
			</div>
			<p><?= $row['description'] ?> </p>
			
			<p class="categories">
				Categories: 
			<?php 
			//print out categories and check for the damn last comma
				$postCategories = $categoryArray[$row['seoTitle'] ];
				$length = sizeof($postCategories);
				$counter = 1;
				foreach($postCategories as $category) {
				    echo "<a href='category/{$category['categorySeoName']}/1'>" .$category['categoryName']. "</a>";
				    if($counter < $length){
				    	echo ", ";
				    }
				    $counter++;
				}
			?>
			</p>
			<p class="date"><?= $row['date'] ?> </p>

		</div><!-- end post -->

		

<?php

	    }
	} else {
	    echo "0 results";
	}
	//$conn->close();

}
//////////////////////////////////////////////////////end getAllComics()

?>



<?php
//////////////////////////////////////////////////// get single post()
function getPost($seoTitle){


	global $conn;
	$sql = "SELECT * FROM comic_strips WHERE seoTitle = '$seoTitle' ";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
?>	        
	    <div class="post">
			<img src="images/posts/<?= $row['imagePath'] ?>">
			<div class="postTopRow">
				<h2><?= $row['title'] ?></h2>
				<?php include "socialIcons.php"  ?>
			</div>
			<p><?= $row['description'] ?> </p>
			<p class="date"><?= $row['date'] ?> </p>
			
		</div>

		<!-- end post -->



<?php

	    }
	} else {
	    echo "0 results";
	}
	//$conn->close();

}
//////////////////////////////////////////////////////end getPost()
?>


















<?php
//////////////////////////////////////////////////// get all the archive data()
function getAllArchive($page = 0,$amount = 10){
	//turn 1 to 0 so page 1 isnt offset by 1* 10
	if($page == 1){
		$offsetAmount = 0;
	}
	else{
		$offsetAmount = ($page - 1) * $amount;
	}
	

	global $conn;
	$sql = "SELECT * FROM comic_strips ORDER BY id DESC LIMIT {$amount} OFFSET {$offsetAmount}";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
?>	        
		<div class="archive">
			<a href="post/<?= $row['seoTitle'] ?>/">
				<img src="images/archive/<?= $row['imagePath'] ?>">
			</a>
			<p>
				<a href="post/<?= $row['seoTitle'] ?>/"><?= $row['title'] ?></a>
			</p>
			<p class="date"><?= $row['date'] ?></p>
		</div>

		<!-- end archive entry -->



<?php

	    }
	} else {
	    echo "0 results";
	}
	//$conn->close();

}
//////////////////////////////////////////////////////end archive data()
?>

















<?php
//////////////////////////////////////////////////////get random comics for the sidebar area
function random(){

	global $conn;
	$sql = "SELECT * FROM comic_strips ORDER BY RAND() LIMIT 10";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
?>	        
		<p>
			<img src="images/post-icons/<?= $row['imagePath'] ?>">
			<a href="post/<?= $row['seoTitle'] ?>/"><?= $row['title'] ?></a>
		</p>

		<!-- end post -->

<?php

	    }
	} else {
	    echo "0 results";
	}
	//$conn->close();

}
//////////////////////////////////////////////////////end random()
?>


