<?php 

$currentURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";



if (strpos($currentURL, 'localhost') !== false) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$dev = true;
	$baseHref = "http://localhost:8888/lostcause/";
}
else{
	$dev = false;
	$baseHref = "http://lostcausecomic/";
}



?>