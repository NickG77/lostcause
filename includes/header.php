<!DOCTYPE html>
<html>
<head>

	
	
	<?php include "includes/devAndProd.php" ?>
	<?php include "includes/database.php"; ?>
	<?php include_once("includes/constants.php");?>
	<?php include "includes/pagination.php"; ?>
	<?php include "includes/getCategories.php"; ?>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?= $baseHref ?>" >

	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Paytone+One&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script
	  src="https://code.jquery.com/jquery-3.4.1.min.js"
	  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="js/scripts.js"></script>

	<title>Lost Cause</title>

	<?php include "includes/favicons.php" ?>
	


</head>



<body>

<div class="mobileHeader">
	<a href="/"><img src="images/mobileLogo.jpg" class="logo"></a>
	<div class="mobileNavButton"></div>

	<div class="mobileNav">
		<p>
			<div class="mobileNavSocial">
				<a href=""><img src="images/socialIcons/instagram.jpg"></a>
				<a href=""><img src="images/socialIcons/tumblr.jpg"></a>
				<a href=""><img src="images/socialIcons/deviantArt.jpg"></a>
				<!-- <a href=""><img src="images/socialIcons/facebook.jpg"></a> -->
			</div>
		</p>

		<p><a href="archive.php">Archive</a></p>
		<p><a href="about.php">About</a></p>
	</div>

</div><!-- end mobile header-->








<div class="headerWide">
	<div class="header"><img src="images/mainHeader.jpg"></div>
</div>

<div class="navBarContainer">
	<a href="index.php" class="navButton">Home</a>
	<a href="archive.php" class="navButton">Archive</a>
	<a href="about.php" class="navButton">About</a>

</div>