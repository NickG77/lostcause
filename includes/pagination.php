

<?php 


function pagination($archiveOrPost,$amount){

if(!empty($_GET['category'])){
	$category = $_GET['category']; 
}


echo "<div class='pagination'>";


$aheadAndBehind = 2;
$buttonAmount = 5;


////////////////////////right now get rid of the 'next' and 'prev' arrows in archive

if($archiveOrPost == "archive"){
	$baseLink = "archive/";
	$totalPageAmount = round(TOTAL_COMICS_AMOUNT / $amount);
}
else if($archiveOrPost == "category"){
	$baseLink = "category/{$category}/";
	$amountOfThisCategory = getCategoryPostAmount($category);
	$totalPageAmount = ceil($amountOfThisCategory / $amount);
}
else{
	$baseLink = "";
	$totalPageAmount = round(TOTAL_COMICS_AMOUNT / $amount);
}




if($archiveOrPost == "post"){
//////////////////////////////////////////////////add the 'previous page' arrow
	if(CURRENT_PAGE != 1){
		$prevPage = CURRENT_PAGE - 1;
		echo "<a href='{$baseLink}{$prevPage}/' class='button' title='Previous'> < </a>";
	}

	///////////////////////////////////////////////add the '...' and the last page if its not visible
	if(CURRENT_PAGE - $aheadAndBehind > 1){
		
		echo "<a  href='1/' class='button' title='page 1'> 1 </a>";
		echo "<a class='button dots'> ... </a>";
	}

}//////end if() checking for is we're on normal posts to show the arrows





//////////////////////////////////////////////if your viewing a category and theres like 2 pages
if($totalPageAmount < 5){
	for($i = 1; $i <= $totalPageAmount; $i++){
		if($i == CURRENT_PAGE){
			echo "<a  href='{$baseLink}{$i}/' class='button selected'  title='page {$i}'>{$i}</a>";
		}
		else{
			echo "<a href='{$baseLink}{$i}/' class='button'  title='page {$i}'>{$i}</a>";
		}
	}//end for()
}//end if()






//////////////////////////////////////////////if the page is the first 4 
else if(CURRENT_PAGE - $aheadAndBehind <= 1 ){
	for($i = 1; $i <= $buttonAmount; $i++){
		if($i == CURRENT_PAGE){
			echo "<a  href='{$baseLink}{$i}/' class='button selected'  title='page {$i}'>{$i}</a>";
		}
		else{
			echo "<a href='{$baseLink}{$i}/' class='button'  title='page {$i}'>{$i}</a>";
		}
	}//end for()
}//end if()







//////////////////////////////////////////////if the page is less than 3 away from the end
else if(CURRENT_PAGE + $aheadAndBehind >= $totalPageAmount){
	$startPoint = $totalPageAmount - $buttonAmount;

	for($i = $startPoint; $i <= $totalPageAmount; $i++){
		if($i == CURRENT_PAGE){
			echo "<a href='{$baseLink}{$i}/' class='button selected'  title='page {$i}'>{$i}</a>";
		}
		else{
			echo "<a href='{$baseLink}{$i}/' class='button'  title='page {$i}'>{$i}</a>";
		}
	}//end for()
}//end if()







//////////////////////////////////////////////if the page is one of the many in the middle
else{
	$startPoint = CURRENT_PAGE - $aheadAndBehind;
	$endPoint = CURRENT_PAGE + $aheadAndBehind;
	for($i = $startPoint; $i <= $endPoint; $i++){
		if($i == CURRENT_PAGE){
			echo "<a href='{$baseLink}{$i}/' class='button selected'  title='page {$i}'>{$i}</a>";
		}
		else{
			echo "<a href='{$baseLink}{$i}/' class='button'  title='page {$i}'>{$i}</a>";
		}
	}//end for()
}









if($archiveOrPost == "post"){

	///////////////////////////////////////////////add the '...' and the last page if its not visible
	if(CURRENT_PAGE + $aheadAndBehind < $totalPageAmount){
		echo "<a class='button dots'> ... </a>";
		echo "<a href='{$baseLink}{$totalPageAmount}/' class='button' title='page {$totalPageAmount}'> {$totalPageAmount} </a>";
	}




	//////////////////////////////////////////////////add the 'next page' arrow
	if(CURRENT_PAGE < $totalPageAmount){
		$nextPage = CURRENT_PAGE + 1;
		echo "<a href='{$baseLink}{$nextPage}/' class='button'  title='next'> > </a>";
	}
}






echo "</div>"; //end of the whole pagination div

}

?>















