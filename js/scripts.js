$(document).ready(function(){

  $(".mobileNavButton").click(function(){
    $(".mobileNav").stop().slideToggle('fast','linear');
  });



////////////////////////////////random episode rollover animation

  $(".randomContainer p a").mouseenter(function(){
     //$(this).parent().find('img').css({"display":"block","opacity":1});
     $(this).siblings().css({"display":"block"});
     $(this).siblings().stop().animate({
     	queue: false,
     	"left":"-68px",
     	"opacity":1,
     	"cursor":"pointer"
     },200);
  });

  $(".randomContainer p a").mouseleave(function(){
     //dont need to set it back to display:none since the img has no pointer events
     //dont know why the animation on the way back is so fast
     $(this).siblings().stop().animate({
     	queue: false,
     	"left":"-10px",
     	"opacity":0
     },200);
  });

////////////////////////////////END random episode rollover animation



////////////////////////////////hover animation effect for the social media icons 
  $(".spreadTheCause p").mouseenter(function(){
     //$(this).parent().find('img').css({"display":"block","opacity":1});
     $(this).children().stop().animate({"margin-top":0},100);
  });

  $(".spreadTheCause p").mouseleave(function(){
     //$(this).parent().find('img').css({"display":"block","opacity":1});
     $(this).children().stop().animate({"margin-top":"4px"},100);
  });



  //////////////////////////////////remove the last posts bottom border
  $(".post").last().css("border", 0);




  ///////////////////////////////////pagination hack fix for the fact that archives dont have enough pages yet
  //////////////////////////////////if youre at the end, it will display page [0].
  //////////////////////////////////just remove the button with 0 in it 


  var archiveURL = window.location.href;
  if(archiveURL.indexOf('archive') != -1){

     $( ".pagination>.button" ).each(function() {
      var zeroCheck = $(this).text();
      console.log(zeroCheck + " : " + zeroCheck.indexOf('0'));
      if(zeroCheck.indexOf('0') != -1){
        $(this).remove();
      }
    }); 

  }






});//end doc ready()


