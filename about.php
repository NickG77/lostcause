<?php include "includes/header.php" ?>






<div class="aboutContainer">
	


		<h1 class="aboutHeader">About Lost Cause</h1>
		<div class="aboutSocialIconsContainer">
			<?php include "includes/spreadTheCause.php"; ?>

		</div>


		<div class="aboutContent">

		<p>
			<img src='images/aboutHeadshot.jpg' align='left' />
			<span>Our Protagonist</span>
			'Lost Cause' follows Midge through the fantastic and the mundane. Stuck in a career he hates, with no hope for any advancement or escape, his favorite coping mechanisms include alcohol, pill and porn addictions mixed with constant fantasies of suicide. 
			<br>
			While living in a world shared with various aliens, monsters, and heroes he can never hope to compete with... things are made even worse upon the realization that they too, fill their time with pointless day jobs and existential dread.   
			<br>
			With his childhood dreams and best years behind him, Midge wastes away his days working and searching for a reason for tomorrow. 
			In all his nihilistic ponderings, he always fails to ask the most important question. 
			Does he really inhabit a world of super heroes and the supernatural? Or are they all just the desperate, drug induced dreams of a depressed and deluded mind? 

		</p>
		</div>




		<div class="aboutContent">
		<p>
			<img src='images/aboutHeadshotNick.jpg' align='left' />
			<span>The Creator</span>
			Hi. My name is Nick Georgiou and I am the creator of 'Lost Cause'. I'm originally from Long Island, NY where multiple failed careers have somehow brought me to Los Angeles, working a job writing web code where I fantasize daily about throwing myself out my 9th floor office windows. 
			<br>
			'Lost Cause' is a silly side project I've been doing on and off for a small group of friends for a while. It started as 'Quarter Life Crisis' when we were about 25-ish, and has 'matured' into it's current iteration.  Back then the comic was about the real world crushing your dreams as you get out of college, friends getting married, dating and all that stuff. 
			<br>
			Now, in my EARLY 40s, I hope to make it about coping with the fact that your dreams have been crushed. I want to cover how people deal with the fact that they're living lives they never intended and there's nothing on the horizon. My goal is to write about various interesting thoughts about the human condition, but let's be honest. I'll be shocked if less than 80% of this thing is nothing but dick jokes.
			<br> 
			Seriously, thanks so much for taking an interest in 'Lost Cause'. I hope you enjoy it.
		</div>

		







	




</div><!-- end container-->



</body>
</html>